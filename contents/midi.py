#!/usr/bin/env python3
"""
MIDI Functions for Rutherford.

Author: Felix Chenier
November 2019
"""
import subprocess
import os
import pathlib

dir = str(pathlib.Path(__file__).parent.absolute())

# MIDI COMMUNICATION METHODS
def send_to_atom(string_to_send):
    """Send a MIDI string to ATOM"""
    _atom_sender.stdin.write(string_to_send + '\n')
    _atom_sender.stdin.flush()


def send_to_daw(string_to_send):
    """Send a MIDI string to DAW"""
    _daw_sender.stdin.write(string_to_send + '\n')
    _daw_sender.stdin.flush()


def read_from_atom():
    """Read a MIDI string from ATOM"""
    return _atom_receiver.stdout.readline()


# ---------------------
# Module initialization

# Open MIDI pipes
dir_path = os.path.dirname(os.path.realpath(__file__))

_atom_receiver = subprocess.Popen(
    [dir + "/receivemidi", "nn", "dev", "ATOM", "--"],
    stdin=subprocess.PIPE, stdout=subprocess.PIPE, universal_newlines=True)

_atom_sender = subprocess.Popen(
    [dir + "/sendmidi", "dev", "ATOM", "--"],
    stdin=subprocess.PIPE, stdout=subprocess.PIPE, universal_newlines=True)

_daw_sender = subprocess.Popen(
    [dir + "/sendmidi", "virt", "Rutherford", "--"],
    stdin=subprocess.PIPE, stdout=subprocess.PIPE, universal_newlines=True)
