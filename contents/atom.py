#!/usr/bin/env python3
"""
Functions related to ATOM LED control

Author: Felix Chenier
Date: November 2019
"""

import contents.midi as midi


def set_pad_color(i_pad, red, green, blue):
    """Set i_pad to RGB color, where RGB are ints between 0 and 127."""
    # White balance
    green = int(green * (100/127))
    blue = int(blue * (35/127))
    # Send
    midi.send_to_atom(f'ch 1 on {i_pad + 36} 127')
    midi.send_to_atom(f'ch 2 on {i_pad + 36} {red}')
    midi.send_to_atom(f'ch 3 on {i_pad + 36} {green}')
    midi.send_to_atom(f'ch 4 on {i_pad + 36} {blue}')


def set_button_state(i_cc, boolean):
    """Highlight (if True) the button corresponding to control change i_cc."""
    if boolean:
        midi.send_to_atom(f'ch 1 control-change {i_cc} 127')
    else:
        midi.send_to_atom(f'ch 1 control-change {i_cc} 0')


def set_managed_mode(boolean):
    """True to set ATOM in managed (dumb) mode, False to revert to default."""
    if boolean:
        midi.send_to_atom('ch 16 off C-2 127')
    else:
        midi.send_to_atom('ch 16 off C-2 0')
