#!/usr/bin/env python3

"""
Rutherford worker code

@author: Felix Chenier
@date: May 2019

This script comes with no warranty.

felix@felixchenier.com
"""

# Standard
import threading
import sys
import os
import math
import colorsys

# Rutherford
import contents.dictionary as dictionary
import contents.midi as midi
import contents.atom as atom


default_pad_color = [80, 40, 5]     # red, green, blue
#note_on_pad_color = [127, 100, 30]  # red, green, blue
note_on_pad_color = [127, 127, 127]  # red, green, blue


class RutherfordWorker(threading.Thread):

    # -----------------
    # THREAD INITIATION
    # -----------------
    def __init__(self, gui_handles):

        # Parent class init
        threading.Thread.__init__(self)

        # --------------------
        # Self variables init:
        # stillrunning - Set to False to stop the thread
        self.stillrunning = True
        # Handles to update the GUI
        self.gui_handles = gui_handles
        # Preference Folder
        self.preference_folder = (os.path.expanduser('~') +
                                  '/Library/Application Support/' +
                                  'com.felixchenier.rutherford')
        # Current state
        self.current_mode = 'normal' # 'normal', 'setup' or 'bank'
        self.previous_bank = 15
        self.selected_bank = 0
        self.selected_pad = 0
        self.shift_pressed = 0
        self.knob_divider = 0   # increases at each knob tick in setup mode
                                # and resets at a given value.

        # Default note remap
        self.note_map = [list(range(36,52)), list(range(52,68)),
                         list(range(68,84)), list(range(84,100)),
                         list(range(100,116)), list(range(116,132)),
                         list(range(4,20)), list(range(20,36)),
                         list(range(36,52)), list(range(52,68)),
                         list(range(68,84)), list(range(84,100)),
                         list(range(100,116)), list(range(116,132)),
                         list(range(4,20)), list(range(20,36))]
        # Default note toggle config (True for Toggle, False for Fire)
        self.note_toggle_config = [[False] * 16] * 16
        # Default hue
        self.note_hue_config = [[_] * 16 for _ in range(16)]

        # Default and selected pad color
        self.default_pad_colors = [(0, 0, 0)] * 16
        self.selected_pad_colors = [(0, 0, 0)] * 16
        # Default note state
        self.note_on_state = [False] * 128

        # ---------------------
        # Initialization functions:

        # Load settings (overwrite default note_map)
        sys.path.append(self.preference_folder)
        try:
            import settings
            self.note_map = settings.note_map
            self.note_toggle_config = settings.note_toggle_config
            self.note_hue_config = settings.note_hue_config
        except Exception:
            # If we could not import the settings, maybe it's simply the first
            # time we run Rutherford. Create the preferences folder.
            try:
                os.mkdir(self.preference_folder)
            except Exception:
                # If we couldn't, it's ok, but we won't be able to save
                # anything.
                pass

        # Set ATOM managed (dumb) mode:
        atom.set_managed_mode(True)

        # Set normal mode
        self.set_mode('normal')
        self.update_gui()

    def save_settings(self):
        """Save settings in the Application Preferences folder."""
        fid = open(self.preference_folder + "/settings.py", "w")
        fid.write(f'note_map = {str(self.note_map)}')
        fid.write('\n')
        fid.write(f'note_toggle_config = {str(self.note_toggle_config)}')
        fid.write('\n')
        fid.write(f'note_hue_config = {str(self.note_hue_config)}')
        fid.close()

    def reset_pad_color(self, i_pad):
        """Reset pad i_pad to its default color."""
        if self.current_mode == 'normal':
            if self.note_toggle_config[self.selected_bank][i_pad]:
                # Toggle mode
                i_note = self.note_map[self.selected_bank][i_pad]
                if self.note_on_state[i_note]:
                    atom.set_pad_color(i_pad, *self.selected_pad_colors[i_pad])
                else:
                    atom.set_pad_color(i_pad, *self.default_pad_colors[i_pad])
            else:
                # Trigger mode
                atom.set_pad_color(i_pad, *self.default_pad_colors[i_pad])
        else:
            atom.set_pad_color(i_pad, *self.default_pad_colors[i_pad])

    def reset_toggle_pad_color(self, i_note):
        """Reset all toggle pads color that are mapped to note number i_note"""
        for i_pad in range(16):
            if (self.note_toggle_config[self.selected_bank][i_pad] and
                    i_note == self.note_map[self.selected_bank][i_pad]):
                self.reset_pad_color(i_pad)

    def get_note_str(self, i_note):
        """Return a string ("C#1") corresponding to a note integer"""
        octave = int(math.floor(i_note/12) - 2)
        i_note = i_note % 12
        return(dictionary.note_names[i_note] + str(octave))

    def update_default_pad_color(self):
        """Update default pad color based on note remap."""
#        current_map = self.note_map[self.selected_bank]
        for i_pad in range(16):
            hue_config = self.note_hue_config[self.selected_bank][i_pad]
            if hue_config < 16:
                hue = hue_config / 16
                sat = 1
                value = 1
            elif hue_config < 32:
                hue = (hue_config - 16) / 16
                sat = 0.75
                value = 0.85
            elif hue_config == 32:
                # White
                hue = 0
                sat = 0
                value = 1
            elif hue_config == 33:
                # Grey
                hue = 0
                sat = 0
                value = 0.75
            else:
                # Off
                hue = 0
                sat = 0
                value = 0

            if self.note_toggle_config[self.selected_bank][i_pad]:
                dark_multiplier = 0.15
            else:
                dark_multiplier = 0.5

            self.default_pad_colors[i_pad] = list(colorsys.hsv_to_rgb(
                    hue, sat, value * dark_multiplier))
            self.selected_pad_colors[i_pad] = list(colorsys.hsv_to_rgb(
                    hue, sat, value))

            for i_color in range(3):
                self.default_pad_colors[i_pad][i_color] = int(
                        self.default_pad_colors[i_pad][i_color] * 127)
                self.selected_pad_colors[i_pad][i_color] = int(
                        self.selected_pad_colors[i_pad][i_color] * 127)


    def note_on(self, i_note, velocity):
        """Send note on to daw, and update internal note_on_state."""
        midi.send_to_daw(f'channel\t10\tnote-on\t{i_note}\t{velocity}')
        self.note_on_state[i_note] = True

    def note_off(self, i_note, velocity):
        """Send note on to daw, and update internal note_on_state."""
        midi.send_to_daw(f'channel\t10\tnote-off\t{i_note}\t0')
        self.note_on_state[i_note] = False

    # -----------
    # MODE SETTER
    # -----------
    def set_mode(self, mode):

        self.current_mode = mode

        if mode == 'normal':
            # Turn off CC
            for i_cc in range(0, 128):
                atom.set_button_state(i_cc, False)
            # Update pad color.
            self.update_default_pad_color()
            for i_pad in range(16):
                self.reset_pad_color(i_pad)

        elif mode == 'bank':
            # Turn off CC
            for i_cc in range(0, 128):
                atom.set_button_state(i_cc, False)
            # Turn on bank button
            atom.set_button_state(dictionary.button_cc['Bank'], True)
            # Update pad colors.
            self.update_default_pad_color()
            # Reset pad colors.
            for i_pad in range(16):
                self.reset_pad_color(i_pad)
            # Turn on the selected bank pad
            atom.set_pad_color(self.selected_bank,
                               *note_on_pad_color)

        elif mode == 'setup':
            # Turn off CC
            for i_cc in range(0, 128):
                atom.set_button_state(i_cc, False)
            # Turn on setup button
            atom.set_button_state(dictionary.button_cc['Setup'], True)
            # Reset pad colors.
            for i_pad in range(16):
                self.reset_pad_color(i_pad)
            # Turn on the selected bank pad
            atom.set_pad_color(self.selected_pad,
                               *self.selected_pad_colors[self.selected_pad])

        else:
            raise ValueError('This mode is not understood.')

    # ----------
    # GUI UPDATE
    # ----------
    def update_gui(self):
        for i_pad in range(16):
            pad_note = self.note_map[self.selected_bank][i_pad]

            if self.current_mode == 'setup' and i_pad == self.selected_pad:
                if self.note_toggle_config[self.selected_bank][i_pad]:
                    selected_text = 'SETUP\nTOGGLE'
                else:
                    selected_text = 'SETUP\nTRIG'
            else:
                selected_text = '\n'

            pad_text = (selected_text + '\n' + self.get_note_str(pad_note) +
                        '\n\n' + dictionary.drum_names[pad_note])
            self.gui_handles['pad' + str(i_pad) + 'Text'].set(pad_text)

        self.gui_handles['TopLabelText'].set(
                'Bank ' + str(self.selected_bank + 1))

    # -------------------------------------------------------------
    # PROCESS LINE (MAIN PROCESSING FUNCTION CALLED FROM MAIN LOOP)
    # -------------------------------------------------------------
    def process_line(self, line):

        tokens = line.split()
        event = tokens[2]

        if event == "note-on" or event == "note-off":
            note = int(tokens[3])
            velocity = int(tokens[4])
            i_pad = note - 36
            i_note = self.note_map[self.selected_bank][i_pad]

        elif event == "control-change":
            cc = int(tokens[3])
            value = int(tokens[4])

        # -----------
        # NORMAL MODE
        if self.current_mode == 'normal':

            if event == "note-on":
                if self.shift_pressed:
                    midi.send_to_daw("channel\t1\tcontrol-change\t"
                                     + str(i_pad + 42) + "\t127")

                elif self.note_toggle_config[self.selected_bank][i_pad]:
                    # Toggle
                    self.note_on_state[i_note] = not self.note_on_state[i_note]
                    if self.note_on_state[i_note]:
                        self.note_on(i_note, velocity)
                    else:
                        self.note_off(i_note, velocity)
                    self.reset_toggle_pad_color(i_note)
                else:
                    # Trigger
                    self.note_on(self.note_map[self.selected_bank][i_pad],
                                 velocity)
                    self.reset_toggle_pad_color(i_note)
                    atom.set_pad_color(i_pad, *note_on_pad_color)

            elif event == "note-off":

                if not self.note_toggle_config[self.selected_bank][i_pad]:
                    # Trigger
                    self.note_off(i_note, velocity)
                    self.reset_toggle_pad_color(i_note)
                    self.reset_pad_color(i_pad)

            elif event == "control-change":

                if (cc == dictionary.button_cc['Setup']) & (value == 127): #Jump to setup mode
                    self.set_mode('setup')
                elif (cc == dictionary.button_cc['Bank']) & (value == 127): #Jump to bank mode
                    self.set_mode('bank')
                    self.update_gui()
                elif (cc == dictionary.button_cc['PreviousBank']) & (value == 127): #Jump to bank mode
                    temp = self.previous_bank
                    self.previous_bank = self.selected_bank
                    self.selected_bank = temp
                    self.set_mode('normal')
                    self.update_gui()
                elif (cc == dictionary.button_cc['Shift']) & (value == 0):
                    self.shift_pressed = 0
                elif (cc == dictionary.button_cc['Shift']) & (value == 127):
                    self.shift_pressed = 1
                else:
                    midi.send_to_daw("channel\t1\tcontrol-change\t"
                                   + str(cc + 10*self.shift_pressed) + "\t" + str(value))

            else:
                midi.send_to_daw(line)

        # ---------
        # BANK MODE
        elif self.current_mode == 'bank':

            if event == "note-on":

                self.previous_bank = self.selected_bank
                self.selected_bank = i_pad
                self.set_mode('bank')

            elif event == "control-change":

                if (cc == dictionary.button_cc['Bank']) & (value == 0):
                    self.set_mode('normal')

            self.update_gui()

        # ----------
        # SETUP MODE
        else:

            if event == "note-on":

                self.note_on(self.note_map[self.selected_bank][i_pad],
                             velocity)
                self.selected_pad = i_pad
                # Update pad colors.
                self.update_default_pad_color()
                # Reset pad colors.
                for _ in range(16):
                    self.reset_pad_color(_)
                # Turn on the selected bank pad
                atom.set_pad_color(self.selected_pad,
                                   *self.selected_pad_colors[
                                           self.selected_pad])

            elif event == "note-off":

                self.note_on(self.note_map[self.selected_bank][i_pad],
                             velocity)

            elif event == "control-change":

                if (cc == dictionary.button_cc['Setup']) & (value == 127): # Exit setup mode
                    self.set_mode('normal')

                elif (cc == 14):
                    self.knob_divider = self.knob_divider + 1
                    if self.knob_divider > 2:
                        self.knob_divider = 0
                        temp = self.note_map[self.selected_bank][self.selected_pad]
                        if (value < 37): # 1 = +, 65 = -
                            temp = temp + 1
                            if temp > 127:
                                temp = 127
                            self.note_map[self.selected_bank][self.selected_pad] = temp
                        else:
                            temp = temp - 1
                            if temp < 0:
                                temp = 0
                            self.note_map[self.selected_bank][self.selected_pad] = temp

                        self.note_on(self.note_map[self.selected_bank]
                                                  [self.selected_pad], 50)

                        # Update pad colors.
                        self.update_default_pad_color()
                        # Turn on the selected bank pad
                        atom.set_pad_color(self.selected_pad,
                                           *self.selected_pad_colors[self.selected_pad])

                        self.note_off(self.note_map[self.selected_bank]
                                                  [self.selected_pad], 50)

                        self.save_settings()

                elif (cc == 15):
                    self.knob_divider = self.knob_divider + 1
                    if self.knob_divider > 2:
                        self.knob_divider = 0
                        self.note_toggle_config[self.selected_bank][
                                self.selected_pad] = (
                                not self.note_toggle_config[
                                        self.selected_bank][self.selected_pad])
                        self.save_settings()

                elif (cc == 17):
                    self.knob_divider = self.knob_divider + 1
                    if self.knob_divider > 2:
                        self.knob_divider = 0
                        if (value < 37):  # 1 = +, 65 = -
                            self.note_hue_config[self.selected_bank][
                                    self.selected_pad] += 1
                            if self.note_hue_config[self.selected_bank][
                                    self.selected_pad] > 34:
                                self.note_hue_config[self.selected_bank][
                                    self.selected_pad] = 0
                        else:
                            self.note_hue_config[self.selected_bank][
                                    self.selected_pad] -= 1
                            if self.note_hue_config[self.selected_bank][
                                    self.selected_pad] < 0:
                                self.note_hue_config[self.selected_bank][
                                    self.selected_pad] = 34
                        # Update pad colors.
                        self.update_default_pad_color()
                        atom.set_pad_color(self.selected_pad,
                                           *self.selected_pad_colors[
                                                   self.selected_pad])
                        self.save_settings()

            self.update_gui()



    # -----------------
    # THREAD MAIN LOOP
    # -----------------
    def run(self):
        while True:
            line = midi.read_from_atom()
            self.process_line(line)
