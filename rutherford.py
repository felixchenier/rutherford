#!/usr/bin/env python3
"""
Rutherford

Main file

@author: Félix Chénier
@date: June 2019
"""
import tkinter as tk
from tkinter import messagebox
from functools import partial
import os
import contents.worker as worker
import contents.atom as atom

# GUI Functions
def closegui(root):
    root.destroy()
    root.quit()

def on_quit():
    closegui(root)

def eventbuttonclicked(buttonID):
    pass

def about_box():
    message = '''Rutherford, version 1.0.

Author: Félix Chénier, 2019.

This software uses the incredibly useful SendMIDI and ReceiveMIDI commandline tools, by
Geert Bevin:
- https://github.com/gbevin/SendMIDI
- https://github.com/gbevin/ReceiveMIDI

Distribution is done with pyinstaller.

This software comes with absolutely no warranty. Please check this website for help:
http://rutherford.felixchenier.com
'''
    messagebox.showinfo('About Rutherford', message)

def creategui():
    root = tk.Tk()

    # About menu
    root.createcommand('tkAboutDialog', about_box)

    # Add menu bar
    menubar = tk.Menu()
    root.configure(menu=menubar)

    # create a File menu and add it to the menubar
    file_menu = tk.Menu(menubar, tearoff=False)
    menubar.add_cascade(label="Help", menu=file_menu)
    file_menu.add_command(label="About Rutherford", command=about_box)

    gui_handles = dict()
    gui_handles['TopLabelText'] = tk.StringVar()
    for i in range(0,16):
        gui_handles['pad' + str(i) + 'Text'] = tk.StringVar()

    root.title("Rutherford")

    # Dark mode
    root.configure(background='black')

    # Create the main window (3 rows of panels)
    frame_top = tk.Frame(root)
    frame_top.pack(fill=tk.X)
    frame_center = tk.Frame(root)
    frame_center.pack(fill=tk.X)
#    frameBottom = tk.Frame(root).pack(fill=tk.X)

    # Create the contents of the top row
    top_label = tk.Label(frame_top, textvariable = gui_handles['TopLabelText'],
                        background = 'gray25',
                        foreground = 'white')
    top_label.pack(fill=tk.X)

    # Create the contents of the center frame
    for i_row in range(0,4):
        for i_column in range(0,4):
            i_pad = i_row*4 + i_column
            the_button = tk.Label(frame_center,
                textvariable = gui_handles['pad' + str(i_pad) + 'Text'],
                relief = tk.GROOVE,
                border = 1,
                width = 16,
                height = 8,
                background = 'gray25',
                foreground = 'white',
                font=("Arial", 11))
            the_button.grid(column = i_column, row = (3-i_row))

    root.protocol("WM_DELETE_WINDOW", partial(closegui, root)) # Close button returns -1
    root.resizable(False, False)
#    root.wm_attributes("-topmost", 1) # Force topmost
    return([root, gui_handles])


# MAIN FUNCTION
root, gui_handles = creategui()

theworker = worker.RutherfordWorker(gui_handles)
theworker.start()

try:
    root.mainloop()
except:
    pass

atom.set_managed_mode(False)
print('Rutherford quitted. You may now close this console window.')
os._exit(0)
